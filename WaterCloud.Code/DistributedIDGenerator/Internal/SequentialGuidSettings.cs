﻿namespace WaterCloud.Code
{
    /// <summary>
    /// 连续 GUID 配置
    /// </summary>
    public sealed class SequentialGuidSettings
    {
        /// <summary>
        /// 连续 GUID 类型
        /// </summary>
        public SequentialGuidType GuidType { get; set; }
    }
}